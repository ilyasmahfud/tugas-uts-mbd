const { default: axios } = require("axios");
const moment = require("moment");
const router = require(`express`).Router();
const baseUrl = "https://fokusin-backend.herokuapp.com/";

router.get("/agenda", async (req, res) => {
  let dataSemua = [];
  let dataBaru = [];
  if (req.cookies.login_token) {
    let { id, token } = req.cookies.login_token;
    // get tasklist
    try {
      let data = await axios.get(`${baseUrl}tasklist/show`, {
        headers: {
          Authorization: `Bearer ${token}`,
        },
      });
      var dataTask = data.data.data;
    } catch (error) {
      var dataTask = null;
    }
    let date = new Date();
    var month = (date.getMonth() + 1 < 10 ? "0" : "") + (date.getMonth() + 1);
    var year = date.getFullYear();

    for (let index = 0; index < 31; index++) {
      let tanggal = index + 1;
      let datalist = {
        namaList: "none",
      };
      dataSemua[index] = { tanggal: tanggal, datalist: datalist };
      if (dataTask != null) {
        dataTask.forEach((data) => {
          var bulan = moment(data.dateList).format("MM");
          var tahun = moment(data.dateList).format("YYYY");
          if (bulan == month && tahun == year) {
            let tanggalData = moment(data.dateList).format("DD");
            if (data.userId == id) {
              if (tanggalData == tanggal) {
                dataSemua[index] = {
                  tanggal: tanggal,
                  datalist: data,
                };
              }
            }
          }
        });
      }
    }
    const monthNames = [
      "January",
      "February",
      "March",
      "April",
      "May",
      "June",
      "July",
      "August",
      "September",
      "October",
      "November",
      "December",
    ];
    let namaBulan = monthNames[date.getMonth()];
    // res.send(dataSemua);
    res.render(`agenda/index.ejs`, {
      judul: "Agenda",
      css: "agenda/agenda",
      title: "kalender",
      agenda: dataSemua,
      moment: moment,
      bulan: namaBulan,
      tahun: year,
    });
  }
});

// prev or next
// router.get("/agenda/:param", async (req, res) => {
//   let dataSemua = [];
//   let param = req.params.param;

//   if (req.cookies.login_token) {
//     let date = new Date();
//     if (param == "prev") {
//       var month = (date.getMonth() < 10 ? "0" : "") + date.getMonth();
//       var year = date.getFullYear() - 1;
//     } else {
//       var month = (date.getMonth() + 2 < 10 ? "0" : "") + (date.getMonth() + 2);
//       var year = date.getFullYear() + 1;
//     }
//     console.log(month);
//     // let { id, token } = req.cookies.login_token;
//     // // get tasklist
//     // try {
//     //   let data = await axios.get(`${baseUrl}tasklist/show`, {
//     //     headers: {
//     //       Authorization: `Bearer ${token}`,
//     //     },
//     //   });
//     //   var dataTask = data.data.data;
//     //   // console.log(dataTask);
//     // } catch (error) {
//     //   res.send(error);
//     // }

//     // for (let index = 0; index < 31; index++) {
//     //   let tanggal = index + 1;
//     //   let datalist = {
//     //     namaList: "none",
//     //   };
//     //   dataSemua[index] = { tanggal: tanggal, datalist: datalist };
//     //   dataTask.forEach((data) => {
//     //     let bulan = moment(data.dateList).format("MM");
//     //     let tahun = moment(data.dateList).format("YYYY");
//     //     if (bulan == month && tahun == year) {
//     //       let tanggalData = moment(data.dateList).format("DD");
//     //       if (tanggalData == tanggal) {
//     //         dataSemua[index] = {
//     //           tanggal: tanggal,
//     //           datalist: data,
//     //         };
//     //       }
//     //     }
//     //   });
//     // }
//     // // res.send(dataSemua);
//     // const monthNames = [
//     //   "January",
//     //   "February",
//     //   "March",
//     //   "April",
//     //   "May",
//     //   "June",
//     //   "July",
//     //   "August",
//     //   "September",
//     //   "October",
//     //   "November",
//     //   "December",
//     // ];
//     // let namaBulan = monthNames[date.getMonth()];
//     // // res.send(dataSemua);
//     // res.render(`agenda/index.ejs`, {
//     //   judul: "To Do List",
//     //   css: "agenda/agenda",
//     //   title: "kalender",
//     //   agenda: dataSemua,
//     //   moment: moment,
//     //   bulan: namaBulan,
//     //   tahun: year,
//     // });
//   }
// });
module.exports = router;
