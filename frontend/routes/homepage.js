const { default: axios } = require("axios");
const baseUrl = "https://fokusin-backend.herokuapp.com/";
const router = require(`express`).Router();
const moment = require(`moment`);

function getRandomItem(arr) {
  // get random index value
  const randomIndex = Math.floor(Math.random() * arr.length);

  // get random item
  const item = arr[randomIndex];

  return item;
  item = null;
}

//homepage
router.get("/homepage", async (req, res) => {
  if (req.cookies.login_token) {
    let { id, token, username } = req.cookies.login_token;

    try {
      let data = await axios.get(`${baseUrl}motivation/show`);
      var dataRandom = getRandomItem(data.data);
    } catch (error) {
      res.send(error);
    }

    // show top goals
    try {
      let data = await axios.get(`${baseUrl}goals`, {
        headers: {
          Authorization: `Bearer ${token}`,
        },
      });
      var datGoals = data.data;
    } catch (error) {
      var datGoals = null;
    }
    // console.log(datGoals);
    var dataaGoals = [];
    if (datGoals.length < 3) {
      dataaGoals = datGoals;
    } else {
      dataaGoals[0] = datGoals[0];
      dataaGoals[1] = datGoals[1];
      dataaGoals[2] = datGoals[2];
    }
    // console.log(dataaGoals);

    // tasklist today
    try {
      let data = await axios.get(`${baseUrl}tasklist/show`, {
        headers: {
          Authorization: `Bearer ${token}`,
        },
      });
      var dataTask = data.data.data;
      // console.log(dataTask);
    } catch (error) {
      var dataTask = null;
    }
    let date = new Date();
    let tanggal = moment(date).format("DD/MM/YYYY");
    // console.log(tanggal);

    //show user progress
    try {
      let data = await axios.get(`${baseUrl}userGoals/show/?user=${id}`, {
        headers: {
          Authorization: `Bearer ${token}`,
        },
      });
      var goalsData = data.data;
    } catch (error) {
      var goalsData = null;
    }
    // console.log(goalsData);
    if (goalsData != null) {
      var dataProgress = [];
      await goalsData.forEach((element, index) => {
        dataProgress[index] = {
          userId: element.userId,
          goalsId: element.goalsId,
          namaGoal: element.Goal.namaGoal,
        };
      });

      var dataProgressBar = [];
      // data progress bar
      for (let index = 0; index < dataProgress.length; index++) {
        try {
          // https://fokusin-backend.herokuapp.com/progress/myProgress?user=J1rb00zL6-WIjWY_jJ52P&goal=ocUA56RvvcUZfbCyKTxM-
          let data = await axios.get(
            `${baseUrl}progress/myProgress?user=${dataProgress[index].userId}&goal=${dataProgress[index].goalsId}`,
            {
              headers: {
                Authorization: `Bearer ${token}`,
              },
            }
          );
          let progress = eval((data.data.done / data.data.total) * 100);
          dataProgressBar[index] = {
            userId: dataProgress[index].userId,
            goalsId: dataProgress[index].goalsId,
            namaGoal: dataProgress[index].namaGoal,
            data: data.data,
            progress: progress,
          };

          // console.log(data);
        } catch (error) {
          dataProgressBar[index] = {
            userId: dataProgress[index].userId,
            goalsId: dataProgress[index].goalsId,
            namaGoal: dataProgress[index].namaGoal,
            data: "",
          };
          // console.log(error);
        }
      }
    } else {
      var dataProgressBar = {
        userId: null,
        goalsId: null,
        namaGoal: null,
        data: "",
      };
    }

    // console.log(dataProgressBar);

    // render homepage
    res.render(`homepage/homepage.ejs`, {
      judul: "fokus.in",
      css: "homepage/homepage",
      title: "home",
      motivate: dataRandom,
      goals: dataaGoals,
      username: username,
      tanggal: tanggal,
      task: dataTask,
      moment: moment,
      idUser: id,
      dataProgressBar: dataProgressBar,
    });
  } else {
    res.redirect("/signin");
  }
});

//B n K
router.get("/bisnisdankeuangan", async (req, res) => {
  let { id, token, username } = req.cookies.login_token;
  let param = `Bisnis %26 Keuangan`;
  try {
    let data = await axios.get(`${baseUrl}goals/?category=${param}`, {
      headers: {
        Authorization: `Bearer ${token}`,
      },
    });
    var datGoals = data.data;
  } catch (error) {
    res.send(error);
  }

  res.render(`homepage/bisnisdankeuangan.ejs`, {
    judul: "Search Result",
    css: "homepage/searchpage",
    title: "home",
    username: username,
    goals: datGoals,
  });
});

//Teknologi
router.get("/teknologi", async (req, res) => {
  let { id, token, username } = req.cookies.login_token;
  let param = `Teknologi`;
  try {
    let data = await axios.get(`${baseUrl}goals/?category=${param}`, {
      headers: {
        Authorization: `Bearer ${token}`,
      },
    });
    var datGoals = data.data;
  } catch (error) {
    // console.log(error);
  }

  res.render(`homepage/teknologi.ejs`, {
    judul: "Search Result",
    css: "homepage/searchpage",
    title: "home",
    username: username,
    goals: datGoals,
  });
});

//Film dan video
router.get("/filmdanvideo", async (req, res) => {
  let { id, token, username } = req.cookies.login_token;
  let param = `Entertainment`;
  try {
    let data = await axios.get(`${baseUrl}goals/?category=${param}`, {
      headers: {
        Authorization: `Bearer ${token}`,
      },
    });
    var datGoals = data.data;
  } catch (error) {
    // console.log(error);
  }

  res.render(`homepage/filmdanvideo.ejs`, {
    judul: "Search Result",
    css: "homepage/searchpage",
    title: "home",
    username: username,
    goals: datGoals,
  });
});

//Pengembangan diri
router.get("/pengembangandiri", async (req, res) => {
  let { id, token, username } = req.cookies.login_token;
  let param = `Pengembangan Diri`;
  try {
    let data = await axios.get(`${baseUrl}goals/?category=${param}`, {
      headers: {
        Authorization: `Bearer ${token}`,
      },
    });
    var datGoals = data.data;
  } catch (error) {
    console.log(error);
  }

  res.render(`homepage/pengembangandiri.ejs`, {
    judul: "Search Result",
    css: "homepage/searchpage",
    title: "home",
    username: username,
    goals: datGoals,
  });
});

//Desain
router.get("/desain", async (req, res) => {
  let { id, token, username } = req.cookies.login_token;
  let param = `Desain`;
  try {
    let data = await axios.get(`${baseUrl}goals/?category=${param}`, {
      headers: {
        Authorization: `Bearer ${token}`,
      },
    });
    var datGoals = data.data;
  } catch (error) {
    // console.log(error);
  }

  res.render(`homepage/desain.ejs`, {
    judul: "Search Result",
    css: "homepage/searchpage",
    title: "home",
    username: username,
    goals: datGoals,
  });
});

//Lihat Semua
router.get("/lihatsemua", async (req, res) => {
  let { id, token, username } = req.cookies.login_token;
  try {
    let data = await axios.get(`${baseUrl}goals`, {
      headers: {
        Authorization: `Bearer ${token}`,
      },
    });
    var datGoals = data.data;
  } catch (error) {
    // console.log(error);
  }
  // console.log(datGoals);

  res.render(`homepage/lihatsemua.ejs`, {
    judul: "Search Result",
    css: "homepage/searchpage",
    title: "home",
    username: username,
    goals: datGoals,
  });
});

router.post(`/search`, async (req, res) => {
  let dataSearch = [];
  let { id, token, username } = req.cookies.login_token;
  let nama = req.body.nama;
  console.log(nama);
  try {
    // console.log(nama);
    let data = await axios.get(
      // `${baseUrl}goals/?nama=${nama}`,
       `${baseUrl}goals/search/?nama=${nama}`,
      {
        headers: {
          Authorization: `Bearer ${token}`,
        },
      }
    );
    dataSearch = data.data;
    res.render(`homepage/lihatsemua.ejs`, {
      judul: "Search Result",
      css: "homepage/searchpage",
      title: "home",
      username: username,
      goals: dataSearch,
    });
  } catch (error) {
    res.redirect(`/notfound`);
  }
});

//notfound
router.get("/notfound", (req, res) => {
  let { id, token, username } = req.cookies.login_token;
  res.render(`homepage/notfound.ejs`, {
    judul: "Search Result",
    css: "homepage/notfound",
    title: "home",
    username: username,
  });
});

// add user goals
router.get(`/addGoals/:param`, async (req, res) => {
  let { id, token, username } = req.cookies.login_token;
  let goalsId = req.params.param;
  // let date = new Date();
  // let tanggal = moment(date).format("DD/MM/YYYY");
  // console.log(tanggal);
  let data = {
    userId: id,
    goalsId: goalsId,
    startGoal: "12/03/2021",
    endGoal: "12/03/2021",
    isEnrolled: true,
  };
  // console.log(data);

  try {
    let post = await axios.post(
      `${baseUrl}userGoals/add`,
      {
        userId: id,
        goalsId: goalsId,
        startGoal: "12/03/2021",
        endGoal: "12/03/2021",
        isEnrolled: true,
      },
      {
        headers: {
          Authorization: `Bearer ${token}`,
        },
      }
    );
    // console.log(post.data);
    res.redirect(`/goal/milestone/detail/${goalsId}`);
  } catch (error) {
    console.log(error);
    res.redirect(`/homepage`);
  }
});
module.exports = router;
