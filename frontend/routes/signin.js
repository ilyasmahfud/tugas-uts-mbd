const { default: axios } = require("axios");
const cookieParser = require("cookie-parser");
const router = require(`express`).Router();
const baseUrl = "https://fokusin-backend.herokuapp.com";
const expiration = process.env.DB_ENV === "testing" ? 100 : 1000000000000000;

// pages ini akan dipisah, jadi index cuma use2 aja. sisanya tinggal dibuat per file untuk router
// homepage
router.get("/", (req, res) => {
  // let message = ``;
  res.render(`onboarding.ejs`);
});

// auth
router.get("/signin", (req, res) => {
  let message = "";
  res.render(`auth/index.ejs`, { message: message });
});

router.post(`/authIn`, (req, res) => {
  let { username, password } = req.body;
  let message = ``;
  console.log(username, password);
  axios
    .post(`${baseUrl}/login`, {
      username: username,
      password: password,
    })
    .then(
      (response) => {
        if (response.data != `wrong username`) {
          if (response.data != `wrong password`) {
            console.log(`welcome ${username}`);
            res
              .status(200)
              .cookie("login_token", response.data, {
                expires: new Date(Date.now() + expiration),
                secure: false,
                httpOnly: true,
              })
              .redirect(`/homepage`);
          } else {
            message = "wrong Password";
            console.log(`Wrong Password`);
            res.status(401).render(`auth/index.ejs`, { message: message });
          }
        } else {
          message = "wrong username";
          console.log(`wrong username`);
          res.status(401).render(`auth/index.ejs`, { message: message });
        }
      },
      (error) => {
        console.log(error);
        res.status(403).redirect(`/signin`);
      }
    );
});
router.get("/signout", (req, res) => {
  res.clearCookie("login_token").redirect(`signin`);
});
router.get(`/cekCookie`, (req, res) => {
  req.cookies;
  res.status(200).json({
    data: req.cookies.login_token,
  });
  console.log("Cookies: ", req.cookies.login_token);
  console.log("Signed Cookies: ", req.signedCookies);
});
module.exports = router;
