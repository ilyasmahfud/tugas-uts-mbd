const express = require("express");
const app = express();
const router = require(`./routes/index`);
const ejs = require("ejs");
const dotenv = require("dotenv");
const chalk = require("chalk");
const cookieParser = require("cookie-parser");
const methodOverride = require("method-override");
const ejsLint = require("ejs-lint");
dotenv.config({ path: `./.env` });

app.use(express.json());
app.use(express.urlencoded({ extended: true }));
app.use(
  methodOverride(function (req, res) {
    // console.log(req.body);
    if (req.body && typeof req.body === "object" && "_method" in req.body) {
      // look in urlencoded POST bodies and delete it
      var method = req.body._method;
      delete req.body._method;
      // console.log(method);
      return method;
    }
  })
);
app.set(`view engine`, ejs);
app.use(cookieParser());
app.set("views", __dirname + "/views");
app.use(express.static(__dirname + "/public"));
app.use(router);

app.listen(process.env.PORT, () =>
  console.log(
    chalk.bgGreenBright(chalk.black(`Server jalan di Port ${process.env.PORT}`))
  )
);
