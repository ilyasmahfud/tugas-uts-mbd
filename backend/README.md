<a href="https://fokusin.herokuapp.com/">
        <img src="img/fokus.inLogo.png" alt="Logo" width="250" height="97">
</a>

# Fokus.in App

Fokus.in is a web-based smart goals planner platform that facilitates users to achieve their goals. Users can choose a goal and will be assigned the tasks / skill set needed that must be mastered according to the goal chosen and has a progress bar that shows the progress of the user's learning journey. It has a daily motivation to motivate users to achieve their goals, users could make their own goals as well, provides learning references / resources, and has a calendar that shows the tasks’ schedule for the user according to their goals.

## Installation

Instal Dependencies
```
npm install
```
Create database
```
sequelize db:create
```
Migrating
```
sequelize db:migrate
```
Seeding
```
sequelize db:seed:all
```
Running
```
npm run dev
```
